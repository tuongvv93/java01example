package DaoDucQuang;

import java.util.Scanner;

public class BaiSo1 {
    //Bai so 1:
    public static void baiSo1() {
        System.out.println("Nhap so thu nhat: ");
        int a = new Scanner(System.in).nextInt();
        System.out.println("Nhap so thu hai: ");
        int b = new Scanner(System.in).nextInt();
        int tich = a * b;
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        System.out.println("Uoc chung lon nhat la: " + a);
        System.out.println("Boi so chung nho nhat la: " + ((tich) / a));
    }

    public static void main(String[] args) {
        baiSo1();
    }
}
