package DaoDucQuang;

import java.util.Scanner;

public class BaiSo3 {
    public static int tong(int i) {
        int sum = 0;
        int n;
        while (i != 0) {
            n = i % 10;
            sum += n;
            i /= 10;
        }
        return (sum);
    }

    public static void main(String[] args) {
        System.out.println("Nhap so n: ");
        int n = new Scanner(System.in).nextInt();
        System.out.println("Tong cua so n la: " + tong(n));
    }
}
