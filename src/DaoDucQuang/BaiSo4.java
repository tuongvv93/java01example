package DaoDucQuang;

import java.util.Scanner;

public class BaiSo4 {
    public static void phanTichSoNguyenTo() {
        System.out.println("Nhap vao mot so nguyen: ");
        int n = new Scanner(System.in).nextInt();
        System.out.println("Ket qua phan tich duoc: ");
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                System.out.print(i + "*");
                n /= i;
                System.out.println("phan tu n =" + n);
            }
        }
    }

    public static void main(String[] args) {
        phanTichSoNguyenTo();
    }
}
