import org.omg.CORBA.PUBLIC_MEMBER;
import tuongvv.Student;

import java.io.*;

public class Example1 implements Serializable {

    public static void main(String[] args) {
        // ra vào file
//        Scanner scanner = null;
//        try {
//            //scanner = new Scanner(new File("data\\debai1.txt"));
//            File file = new File("a.txt");
//            if (!file.exists()) {
//                try {
//                    file.createNewFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    return;
//                }
//            }
//            InputStream inputStream = new FileInputStream("a.txt");
//
//            byte[] bytes = new byte[1000];
//            String s = new String("");
//            while (true) {
//                int length = inputStream.read(bytes);
//                if (length == -1) {
//                    break;
//                }
//                String str = new String(bytes, 0, length);
//                System.out.println(str);
//            }
//            inputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        // 2.
//        try {
//            readWriteFile();
//        } catch (Exception e) {}


        //3.
        readWriteObject();

    }

    public static void readWriteFile() throws NumberFormatException {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("a.txt")));
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("a.txt")))) {

            String s = bufferedReader.readLine();

            if ("Q".equals(s)) {
                return;
            }
            if ("E".equals(s)) {
                throw new NumberFormatException();
            }
            bufferedWriter.write("\n");
            bufferedWriter.newLine();
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        } catch (Exception e) {
        }
    }

    public static void readWriteObject() {
        Student student = new Student();
        student.setId("1000");
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("b.in"));
             ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("b.in"))){
            objectOutputStream.writeObject(student);
            Student student2 = (Student) objectInputStream.readObject();
            System.out.println(student2.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
