package tuongvv.button;

public class Button {
    private String color;   //  màu sắc của nó
    private boolean isEnable; // nó có ở trạng thái cho phép click hay không
    private boolean isHighLight;  // nó có sáng k?
    private boolean isClicking;  // đang được click



    public Button(String color, boolean isEnable, boolean isHighLight, boolean isClicking) {
        this.color = color;
        this.isEnable = isEnable;
        this.isHighLight = isHighLight;
        this.isClicking = isClicking;
    }

    public Button() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }

    public boolean isHighLight() {
        return isHighLight;
    }

    public void setHighLight(boolean highLight) {
        isHighLight = highLight;
    }

    public boolean isClicking() {
        return isClicking;
    }

    public void setClicking(boolean clicking) {
        isClicking = clicking;
    }

    public void caiDatChoNhanThongTinClick(OnClickListener onClickListener) {
        // logic 1
        int value = onClickListener.Onlick();
        // logic 2
        value += onClickListener.Onlick();
        // logic 3
        value += onClickListener.Onlick();

        // logic gì đó tiếp dựa giá trị của hàm main ban đầu
        System.out.println("value: " + value);
    }
}
