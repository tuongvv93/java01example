package tuongvv;

import java.io.Serializable;

public class Student extends People implements Serializable {
    private String Id;
    private int toan, ly, hoa;

    public Student() {
    }

    public Student(String id) {
        Id = id;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public int getAvg() {
        return toan + ly + hoa;
    }
}
