package tuongvv;

public abstract class People {
    private String name;
    private int age;

    public abstract int getAvg();

    public boolean doDaiHoc() {
        if (getAvg() > 7) return true;
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
