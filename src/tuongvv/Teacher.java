package tuongvv;

public class Teacher extends People {
    private String mst;
    private int sinh, su, dia;

    public Teacher() {
    }

    public String getMst() {
        return mst;
    }

    public void setMst(String mst) {
        this.mst = mst;
    }

    public int getSinh() {
        return sinh;
    }

    public void setSinh(int sinh) {
        this.sinh = sinh;
    }

    public int getSu() {
        return su;
    }

    public void setSu(int su) {
        this.su = su;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    @Override
    public int getAvg() {
        return sinh + su + dia;
    }
}
