package generic;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Map<String, Float> map = new HashMap<>();

        Array<String, Float> arrString = new Array<>();

        Float[] floats = new Float[10];
        for (int i = 0; i < 10; i++) {
            floats[i] = i + 0.1f;
        }
        Arrays.sort(floats, new Comparator<Float>() {
            @Override
            public int compare(Float o1, Float o2) {
                return o2.compareTo(o1);
            }
        });
        arrString.setValues(floats);

        arrString.print();
    }


}
