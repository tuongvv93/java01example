package generic;

public class Array<T, E> {
    private T[] key;
    private E[] values;

    public E[] getValues() {
        return values;
    }

    public void setValues(E[] values) {
        this.values = values;
    }

    public void print() {
        for (E value : values) {
            if (value instanceof String) {
                System.out.println("String value: " + value);
                continue;
            }
            if (value instanceof Double) {
                System.out.println("Double value: " + value);
                continue;
            }
            if (value instanceof Float) {
                System.out.println("Float value: " + value);
                continue;
            }
        }
    }
}
