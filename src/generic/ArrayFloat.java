package generic;

public class ArrayFloat {
    private float[] floats;

    public float[] getFloats() {
        return floats;
    }

    public void setFloats(float[] floats) {
        this.floats = floats;
    }
}
