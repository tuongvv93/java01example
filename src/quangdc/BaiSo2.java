package quangdc;

import java.util.Scanner;

public class BaiSo2 {
    public static void doiCoSo(int n,int base){
        if(n>=base) doiCoSo(n / base, base);
        if(n % base>9) System.out.printf("%c",n%base+55);
        else
            System.out.print((n % base));
    }
    public static void main(String[] args) {
        System.out.println("Nhap n: ");
        int n=new Scanner(System.in).nextInt();
        System.out.println("Nhap co so : ");
        int base=new Scanner(System.in).nextInt();
        System.out.println("So " +n+ " chuyen sang co so " +base+ " thanh: ");
        doiCoSo(n,base);
    }
}
