package quangdc;

import java.util.Scanner;

public class BaiSo6 {
    public static boolean soNguyenTo(int n) {
        if (n < 2) {
            return false;
        }
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print("Nhập so n = ");
        int n = new Scanner(System.in).nextInt();
        System.out.printf("%d số nguyên tố đầu tiên là: \n", n);
        int dem = 0;
        int i = 2;
        while (dem < n) {
            if (soNguyenTo(i)) {
                System.out.print(i + " ");
                dem++;
            }
            i++;
        }
    }
}
