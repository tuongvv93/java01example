package luyentapjava.duong.kiemtra;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Bai2 {
    public static void main(String[] args) throws IOException {
        int []a = new int[90];
        int dem = 0;
        Scanner sc = new Scanner(new File("src/luyentapjava/duong/kiemtra/Bai2.txt"));
        int n = sc.nextInt();
        for(int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }
        for(int j = 1; j < n; j++){
            if(a[j] != a[j-1]){
                dem++;
            }
        }
        System.out.println(dem);
    }
}
