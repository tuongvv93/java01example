package luyentapjava.duong;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class BaiD3 {

    static int n = -1;
    static int[] b = new int[1000000];
    static int dem = 0;

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("src/luyentapjava/duong/D.txt"));
        int ans = 1;
        int[] a = new int[1000000];
        while (true) {
            try {
                n++;
                a[n] = sc.nextInt();
            } catch (Exception e) {
                break;
            }
        }
        kiemtraNgto(a);
        Arrays.sort(b,0,dem-1);
        for(int i=1;i<dem;i++){
            if(b[i]!=b[i-1]){
                ans++;
            }
        }
        System.out.println(ans);
    }

    public static void kiemtraNgto(int[] a) {
        for (int i = 0; i < n; i++) {
            if (a[i] == 2 || a[i] == 3) {
                b[dem] = a[i];
                dem++;
            }
            if (a[i] % 2 == 0) {
                continue;
            }
            int can = (int) Math.sqrt(a[i]);
            boolean okla = true;
            for (int j = 3; j <= can; j += 2) {
                if (a[i] % j == 0) {
                    okla = false;
                    break;
                }
            }
            if(okla){
                b[dem] = a[i];
                dem++;
            }
        }
    }
}
