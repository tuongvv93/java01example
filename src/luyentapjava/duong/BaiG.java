package luyentapjava.duong;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class BaiG {
    static int dem = 0;

    public static void main(String[] args) throws IOException {
        int[][] a = new int[1000][1000];
        int[][] b = new int[1000][1000];
        Scanner sc = new Scanner(new File("src/luyentapjava/duong/G.txt"));
        int soVongLap = sc.nextInt();
        for (int i = 0; i < soVongLap; i++) {
            int n = sc.nextInt();
            for (int h = 0; h < n; h++) {
                for (int c = 0; c < n; c++) {
                    a[h][c] = sc.nextInt();
                    System.out.print(a[h][c] + " ");
                }
                System.out.println(" ");
            }
            System.out.println(" ");
            for (int h = 0; h < n; h++) {
                for (int c = 0; c < n; c++) {
                    b[h][c] = sc.nextInt();
                    System.out.print(b[h][c] + " ");
                }
                System.out.println(" ");
            }
            System.out.println(" ");
            lietke(a, b, n);
            kiemtra(a, b, n);
        }
    }

    private static void lietke(int[][] a, int[][] b, int n) {
        for (int h = 0; h < n; h++) {
            for (int c = 0; c < n; c++) {
                if (a[h][c] == b[h][c]) {
                    dem++;
                }
            }
        }
        System.out.println(dem);
    }

    private static void kiemtra(int[][] a, int[][] b, int n) {
        for (int h = 0; h < n; h++) {
            for (int c = 0; c < n; c++) {
                if (a[h][c] == b[h][c]) {
                    System.out.println(h + " " + c);
                }
            }
        }
    }
}
