package BaiTapThuatToanTuDuy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class B2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a[] = new int[80];
        int count = 0;
        for (int i = 0; i<n;i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 1; i<n;i++) {
            if (a[i]!=a[i-1]) {
                count++;
            }
        }
        System.out.println(count);
    }
}