package luyentapjava.wall;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

public class BaiF {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("F.IN"));
        int soVongLap = sc.nextInt();
        for (int i = 0; i < soVongLap; i++) {
            String number1 = sc.nextLine();
            String number2 = sc.nextLine();
            BigInteger bigInteger1 = new BigInteger(number1);
            BigInteger bigInteger2 = new BigInteger(number2);
            System.out.println(bigInteger1.add(bigInteger2));
        }
    }

}
