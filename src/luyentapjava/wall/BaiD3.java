package luyentapjava.wall;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class BaiD3 {
    static int dem = 0;
    static int MAX = 1000000;

    static Map<Integer, Boolean> mangDanhDau = new HashMap<>();
    static Map<Integer, Boolean> mangDanhDauNgto = new HashMap<>();

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("d.txt"));
        int n = -1;
        int[] a = new int[1000000];
        while (true) {
            try {
                n++;
                a[n] = sc.nextInt();
            } catch (Exception e) {
                break;
            }
        }
        dem = 0;
        sangNguyenTo();
        process(a, n);
    }

    public static void process(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            if (kiemTraNgto(a[i]) && !kiemTraLapLai(a[i])) {
                dem++;
                //System.out.println(" -- >" + a[i]);
            }
        }
        System.out.println(dem);
    }

    private static boolean kiemTraLapLai(int x) {
        if (!mangDanhDau.containsKey(x)) {
            mangDanhDau.put(x, true);
            return false;
        }
        return true;
    }

    private static boolean kiemTraNgto(int x) {
        if (mangDanhDauNgto.containsKey(x)) {
            return false;
        }
        return true;
    }

    private static void sangNguyenTo() {
        int n = (int) Math.sqrt(MAX);
        for (int i = 2; i <= n; i++) {
            int m = MAX/i;
            for (int j = 2; j <= m; j++) {
                mangDanhDauNgto.put(i * j, true);
            }
        }

    }

}