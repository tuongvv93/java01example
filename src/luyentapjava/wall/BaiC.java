package luyentapjava.wall;

import java.io.IOException;
import java.util.Scanner;

public class BaiC {


    public static void main (String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int soVongLap = sc.nextInt();
        for (int i = 0; i < soVongLap; i++){
            int a = sc.nextInt();
            int b = sc.nextInt();
            process(a, b);
        }
    }

    // a b => số nguyên a,b > 0   ;   a,b < 10000  ->   0, 0     --- --  30 10 ----  10000, 10000  ----  0, 10000,   ----   10000, 0,
    //  1 4   => imposible
    //  30, 10
    private static void process(int a, int b) {
        if (a < b) {
            System.out.println("impossible");
            return;
        }
        int sum = a + b;
        if (sum % 2 != 0) {
            System.out.println("impossible");
            return;
        }
        System.out.println((sum/2) + " " + (a - sum/2));
    }
}