package luyentapjava.wall;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class BaiD {
    static int dem = 0;
    static int[] mangDanhDau = new int[99999999];

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("d.txt"));
        int n = -1;
        int[] a = new int[1000000];
        while (true) {
            try {
                n++;
                a[n] = sc.nextInt();
            } catch (Exception e) {
                break;
            }
        }
        dem = 0;
        process(a, n);
    }

    public static void process(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            if (kiemTraNgto(a[i]) && !kiemTraLapLai(a[i])) {
                dem++;
                //System.out.println(" -- >" + a[i]);
            }
        }
        System.out.println(dem);
    }

    private static boolean kiemTraLapLai(int x) {
        if (mangDanhDau[x] == 0) {
            mangDanhDau[x] = 1;
            return false;
        }
        return true;
    }

    private static boolean kiemTraNgto(int x) {
        if (x == 2 || x == 3) {
            return true;
        }
        if (x % 2 == 0) {
            return false;
        }
        //  4 -> căn x
        int can = (int) Math.sqrt(x);
        for (int i = 3; i <= can; i += 2) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }
}