package luyentapjava.wall;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class BaiO {
    public static void nhap(int[][] a, int n, int m) throws IOException {
        Scanner sc = new Scanner(new File("O.IN"));
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = sc.nextInt();
            }
        }
    }

    public static void chuyenVi(int[][] a, int[][] b, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                b[i][j] = a[j][i];
            }
        }
    }

    public static void tich(int[][] a, int[][] b, int n, int m) {
        int[][] c = new int[n][n];
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    c[k][i] += a[i][j] * b[j][i];
                }
                System.out.print(c[k][i] + " ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("O.IN"));
        int soVongLap = sc.nextInt();
        for (int i = 0; i < soVongLap; i++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] a = new int[n][m];
            int[][] b = new int[m][n];
            nhap(a, n, m);
            chuyenVi(a, b, n, m);
            tich(a, b, n, m);
        }
    }
}

