package luyentapjava.wall;

import generic.Array;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BaiB {
    static int n, count = 0;

    static void deQuy(long sum, int vt) {
        if (vt == n / 2) {
            if (n % 2 == 0) {
                if (sum * 2 % 10 == 0) {
                    count++;
                }
                return;
            }
            // so le
            for (int i = 0; i < 10; i++) {
                if ((sum * 2 + i) % 10 == 0) {
                    count++;
                }
            }
            return;
        }
        int a = 0;
        if (vt == 0) {
            a = 1;
        }
        for (int i = a; i < 10; i++) {
            sum += i;
            vt++;
            deQuy(sum, vt);
            vt--;
            sum -= i;
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int soVongLap = sc.nextInt();
        for (int i = 0; i < soVongLap; i++) {
            n = sc.nextInt();
            long sum = 0;
            count = 0;
            deQuy(sum, 0);
            System.out.println(count);
        }
    }

}
