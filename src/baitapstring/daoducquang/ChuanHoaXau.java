package daoducquang;

import java.io.*;
import java.util.Scanner;

public class ChuanHoaXau {
    private String st;

    public ChuanHoaXau(String s) {
        st = s;
    }

    public ChuanHoaXau() {
    }

    String nhapString() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap xau: ");
        st = sc.nextLine();
        return st;
    }


    public void hienThi() {
        System.out.println(st);
    }

    public void chuannHoa() {
        st = st.trim().toLowerCase();
        st = st.replaceAll("\\s+", " ");
        String[] temp = st.split(" ");
        st = "";
        for (int i = 0; i < temp.length; i++) {
            st += String.valueOf(temp[i].charAt(0)).toUpperCase() + temp[i].substring(1);
            if (i < temp.length - 1)
                st += " ";
        }
    }

    public static void main(String[] args) {
        ChuanHoaXau a = new ChuanHoaXau();
        a.nhapString();
        a.chuannHoa();
        System.out.println("Xau sau khi chuan hoa: ");
        a.hienThi();
    }
}
