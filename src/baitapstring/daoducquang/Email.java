package daoducquang;

import java.util.Scanner;

public class Email {
    public static void main(String[] args) {
        boolean flag;
        Scanner sc = new Scanner(System.in);
        do {
            String emailPattern = "\\w+@\\w+[.]\\w+";
            System.out.print("Nhap vao email cua ban(email@address.com): ");
            String input = sc.next();
            flag = input.matches(emailPattern);
            if (!flag) System.out.println("email ko hop le!");
        } while (!flag);
        System.out.println("email hop le");
    }
}
