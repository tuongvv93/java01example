package daoducquang;

import java.util.Scanner;

public class Sdt {
    public static void main(String[] args) {
        System.out.println("Nhap vao mot so dien thoai: ");
        Scanner sc = new Scanner(System.in);
        String sdt = sc.nextLine();
        String regex = "[0-9]+";
        if (sdt.matches(regex) && sdt.length() == 10) {
            System.out.println("Day la sdt");
        } else {
            System.out.println("Day khong phai la so dien thoai");
        }
    }
}
