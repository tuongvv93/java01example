package baitapstring.tuongvv;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;

public class Main {

    public void readFile(String fileName) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)))) {
            int n = Integer.parseInt(bufferedReader.readLine());
            for (int i = 0; i < n; i++) {
                process(bufferedReader.readLine());
            }

        } catch (IOException | NumberFormatException e) {

        }
    }

    private void process(String readLine) {
        FullName fullName = new FullName(readLine);
        System.out.println(fullName.standardizeFullName());
    }

    public static void main(String[] args) {
        new Main().readFile("a.txt");
    }
}
