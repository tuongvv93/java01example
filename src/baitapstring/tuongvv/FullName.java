package baitapstring.tuongvv;

import java.util.StringTokenizer;

public class FullName {
    private String fullName;

    public FullName(String fullName) {
        this.fullName = fullName;
    }

    public String standardizeFullName() {
        String str = fullName;
        if (str == null) {
            return null;
        }
        str = str.trim().toLowerCase()
                .replace("\\s+", " ");

        if (Utils.isEmail(str) || Utils.isPhoneNumber(str)) {
            return str;
        }

        StringBuilder stringBuilder = new StringBuilder();
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            stringBuilder.append(String.valueOf(token.charAt(0)).toUpperCase())
                    .append(token.substring(1)).append(" ");
        }
        return stringBuilder.toString().trim();
    }

    @Override
    public String toString() {
        return "FullName{" +
                "fullName='" + fullName + '\'' +
                '}';
    }
}
